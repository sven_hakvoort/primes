-----------
Ulam Spiral
-----------

---------------------
1: prime.py (Calculator)
---------------------

This script calculates all the prime numbers untill a specific number is reached.
The prime numbers are stored at every 1000th number to prevent data loss in case the programm unexpectedly closes.
The calculation position is also displayed every 1000th number.

----------------------
Command Line Arguments
----------------------

Example: ./prime.py 10**3 prime.txt

- The first argument starts the script

- The second argument sets untill which number the prime numbers should be calculated

- The third argument specifies the filename where the prime numbers should be stored.

-----------------
2: ulam.py (Draw)
-----------------

This script reads the prime numbers from a file. Then calculates for every number where it needs to be placed.
If a number is a prime number, it will be drawn as a black pixel. If it isn't it will be a blank pixel.

----------------------
Command line arguments
----------------------

Example: ./ulam.py 10**3 prime.txt 

- The first argument starts the script
- The second argument specifies the largest number which needs to be used
- The third argument specifies the name of file which contains the prime numbers

If value of second argument is higher than the highest number in the database run prime.py to calculate the necessary numbers, then run ulam.py again.