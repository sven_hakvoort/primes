#!/usr/bin/env python3
"""
Program for drawing a ulam spiral

Args:
    - The number of numbers which need to be drawn

Returns:
    - The whole sequence or the last number
"""
from PIL import Image, ImageDraw
import sys

__author__ = "Sven Hakvoort"


def draw_ulam(coords, seq, size):
    """
    Draw the ulam

    Args:
        coords: Coordinates dict
        seq: The sequence of primes
        size: The size of the image

    Returns:
        - Ulam spiral image

    """
    i = 0
    img = Image.new("1", (size, size), "white")
    draw = ImageDraw.Draw(img)

    dot_size = 0.5
    print("Drawing image")
    for (x, y) in coords:
        i += 1
        if i % 1000 == 0:
            print(i)
        mesg = coords[(x, y)]
        if mesg in seq:
            draw.point([x, y], fill="black")
    img.save("ulam.bmp")
    return


def calc_coords(opened, argv):
    """
    Args:
        opened: The file containing the prime numbers
        argv: Command line arguments

    Returns:
        None
    """
    old = 1
    start = 1
    diff = 2
    seq = opened.readline()
    seq = seq.strip().replace(" ", "").split(",")
    last = int(seq[-2])
    num = eval(argv[2])
    rows = int(num**0.5)
    size = rows
    x = size/2
    y = size/2
    coords = {(x, y): "1"}
    print("Calculating coordinates")
    for r in range(3, rows+1, 2):
        new = r
        for i in range(start+1, new**2+2):
            if i >= last:
                print("No more numbers in database")
                return
            if i == 2:
                x += 1
            # coords = (y += r-1), (x -= r-1), (y -= r-1), (x += r-1)
            elif i <= old + diff:
                y -= 1
            elif old + diff < i <= old + diff*2:
                x -= 1
            elif old + diff*2 < i <= old + diff*3:
                y += 1
            elif old + diff*3 <= i <= old + diff*4+1:
                x += 1
            if i == r**2+1:
                start = i
                old = i-1
                diff += 2
            coords.update({(x, y): str(i)})
    draw_ulam(coords, seq, size)
    return


def main(argv=None):
    if argv is None:
        argv = sys.argv
    out = argv[1]
    # Open file
    opened = open(out, "r")
    # Draw ulam and check if number is in file
    calc_coords(opened, argv)
    # Close file
    opened.close()
    return 1


if __name__ == "__main__":
    sys.exit(main())
