#!/usr/bin/env python3
"""
Program for outputting the Fibonacci number

Args:
    - The number of numbers which need to be checked

Returns:
    - A file with all the prime numbers calculated
"""
import sys
import ast

__author__ = "Sven Hakvoort"


def write_to_file(seq, opened):
    """
    Function which outputs the sequence to file
    Args:
        seq: The sequence of numbers
        opened: Opened file

    Returns:
        -
    """
    seq = ", ".join(seq) + ", "
    opened.write(seq)
    return


def prime(opened, argv):
    """
    Calculate if a number is a prime number
    Args:
        opened: Opened file
        argv: command line arguments

    Returns:
        - 0 if number is not a prime number
        - 1 if number is prime number
    """
    num = eval(argv[1])
    seq = opened.readline()
    start = 1
    try:
        old_seq = ast.literal_eval(seq)
        start = old_seq[-1]
    except SyntaxError:
        old_seq = None
    new_seq = []
    for i in range(start+1, num):
        possible_div = 0
        if i % 1000 == 0:
            print(i)
            if bool(new_seq):
                write_to_file(new_seq, opened)
                new_seq = []
        for x in range(2, i+1):
            if i % x == 0:
                possible_div += 1
            if possible_div == 2:
                break
            if x == i and possible_div == 1:
                if old_seq is None or str(i) not in old_seq:
                    new_seq.append(str(i))
    return


def main(argv=None):
    if argv is None:
        argv = sys.argv
    out = argv[2]
    # Open file
    try:
        opened = open(out, "r+")
    except FileNotFoundError:
        opened = open(out, "w+")
    # Check numbers if they are a prime number and output every 10000 to file
    prime(opened, argv)
    # Close file
    opened.close()
    return 1


if __name__ == "__main__":
    sys.exit(main())
